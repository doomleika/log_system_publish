#!/usr/bin/env python
from os import system as command
import os


working_dir = os.path.dirname(os.path.abspath(__file__))
os.chdir(working_dir)
command("docker-compose build")