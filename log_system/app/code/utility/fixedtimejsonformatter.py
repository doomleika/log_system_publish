import time


from pythonjsonlogger import jsonlogger


class FixedUtcTimeJsonFormatter(jsonlogger.JsonFormatter):
    converter = time.gmtime
    def formatTime(self, record, datefmt=None):
        ct = self.converter(record.created)
        if datefmt:
            raise NotImplementedError("You are not supposed to set custom datefmt with this class")
        else:
            t = time.strftime("%Y-%m-%dT%H:%M:%S", ct)
            s = "%s.%03dZ" % (t, record.msecs)
        return s
