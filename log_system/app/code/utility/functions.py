import random
import time
import string

def chance(prob):
    maybe = random.random()
    if maybe <= prob:
        return True
    return False

def random_string():
    return ''.join(random.SystemRandom().choice(string.ascii_lowercase + string.digits) for _ in range(8))

def wait_0to10sec():
    time.sleep(random.random()*10)
