import unittest
import random

import mock

from log_generator.utility.functions import chance


@mock.patch('random.random', return_value=0.5)
class TestFunctionsChance(unittest.TestCase):
    def test_false(self, mock_method):
        self.assertFalse(chance(0.3))

    def test_true(self, mock_method):
        self.assertTrue(chance(0.6))

    def test_same(self, mock_method):
        self.assertTrue(chance(0.5))

    def test_slightly_below(self, mock_method):
        self.assertFalse(chance(0.49999999))

    def test_slightly_above(self, mock_method):
        self.assertTrue(chance(0.5000001))

    def test_always_true(self, mock_method):
        self.assertTrue(chance(1))

    def test_always_false(self, mock_method):
        self.assertFalse(chance(0))