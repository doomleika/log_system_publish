#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging.handlers
import random

from utility import FixedUtcTimeJsonFormatter, wait_0to10sec, chance


MAY_BET = 0.6
PLAYER_NUM = 100
GAME_INIT = 5

logger = logging.getLogger(__name__)
logger.setLevel(level=logging.INFO)

fmt = "%(levelname)s %(created)f %(asctime)s"

logHandler = logging.StreamHandler()
formatter = FixedUtcTimeJsonFormatter(fmt=fmt)
filelogHandler = logging.handlers.TimedRotatingFileHandler(when='h', filename="json_log.json.log")
logHandler.setFormatter(formatter)
filelogHandler.setFormatter(formatter)

logger.addHandler(logHandler)
logger.addHandler(filelogHandler)


class Game(object):
    def __init__(self, id):
        self.id = id
        self.bets = {'A': {}, 'B': {}}

    def bet(self, which, amount, player_id):
        info = {"game_id": self.id,
                "player_id": player_id,
                "bet": which,
                "bet_amount": amount,}
        if player_id not in self.bets[which]:
            self.bets[which][player_id] = amount
            info['first_bet'] = True
        else:
            self.bets[which][player_id] += amount
            info['first_bet'] = False

        info['total_amount'] = self.bets[which][player_id]

        logger.info(info)

    def draw(self):
        self._pick_winner()
        info = {"game_id": self.id,
                "win_bet": self.win_bet,
                "lose_bet": self.lose_bet,}
        for winner in self.bets[self.win_bet]:
            info={"game_id": self.id,
                  "winner_id": winner,
                  "amount": self.bets[self.win_bet][winner],}
            logger.info(info)
        for loser in self.bets[self.lose_bet]:
            info = {"game_id": self.id,
                    "loser_id": loser,
                    "amount": self.bets[self.lose_bet][loser],}
            logger.info(info)

    def _pick_winner(self):
        self.win_bet = random.choice(['A', 'B'])
        choices = ['A', 'B']
        choices.remove(self.win_bet)
        self.lose_bet = choices[0]
        info = {"game_id": self.id,
                "win_bet": self.win_bet,
                "lose_bet": self.lose_bet,}
        logger.info(info)


players = []
games   = []

if __name__ == '__main__':
    # -- init --
    # Generating Players
    for x in range(0, PLAYER_NUM):
        players.append(x)

    # Generating games
    newest_game = 0
    for game_id in range(0, GAME_INIT):
        games.append(Game(game_id))
        newest_game += 1
    # -- init --


    while True:
        games.insert(0, Game(newest_game))
        newest_game = newest_game + 1

        for player in players:
            # if they bet
            if chance(MAY_BET):
                # player bet x for y amount in game x
                bet = random.choice(['A', 'B'])
                amount = random.random()  * 5000
                game = random.choice(games)
                game.bet(which=bet, player_id=player, amount=amount)

        # game state change
        game = games.pop()
        game.draw()

        wait_0to10sec()
