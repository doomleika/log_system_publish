import os
import sys
import time
import logging
import argparse
import datetime

if __name__ == '__main__':
    # file_name
    index_template_done = "index_template_done"

    # Logging Setup
    logging.basicConfig(level=logging.INFO, format="%(asctime)s[%(levelname)s] %(message)s")

    # Parser setup
    parser = argparse.ArgumentParser()
    parser.add_argument('--force', "-f",
                        help='force curl elasticsearch template',
                        action='store_true')
    args = parser.parse_args()

    if os.path.exists(index_template_done):
        logging.info("File '" + index_template_done + "' exists")
        if not args.force:
            logging.info("Skipping curl statement")
            sys.exit(0)
        logging.info('--force flag set, curling for index template regardless')


    logging.info('Trying to create index template...')
    while True:
        return_value = os.system("curl -s -XPOST 'elasticsearch:9200/_template/template_1' -d @/root/template_1.json")
        if return_value == 0:
            logging.info("index template create successful, creating timestamp file '" + index_template_done + "'")
            with open(index_template_done, "w") as f:
                f.write(str(datetime.datetime.now()))
            break

        logging.info('curl unsuccessful, waiting for one sec')
        time.sleep(1)

